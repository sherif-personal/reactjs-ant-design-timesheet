/**
 * 不是真实的 webpack 配置，仅为兼容 webstorm 和 intellij idea 代码跳转
 * ref: https://github.com/umijs/umi/issues/1109#issuecomment-423380125
 */

module.exports = {
  resolve: {
    alias: {
      '@': require('path').resolve(__dirname, 'src'),
    },
  },
  rules: [{
    test: /\.less$/,
    use: [{
      loader: 'style-loader',
    }, {
      loader: 'css-loader', // translates CSS into CommonJS
    }, {
      loader: 'less-loader', // compiles Less to CSS
     options: {
       modifyVars: {
         'primary-color': '#1DA57A',
         'link-color': '#1DA57A',
         'border-radius-base': '2px',
         // or
         'hack': `true; @import "your-less-file-path.less";`, // Override with less file
       },
       javascriptEnabled: true,
     },
    }],
  }]
};
